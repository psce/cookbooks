#
# Cookbook Name:: psce-dbmon
# Recipe:: default
#
# Copyright 2016, PSCE
#
# All rights reserved - Do Not Redistribute
#

# Lists system packages we depnd on to perform certain work
pkg_deps = value_for_platform({
    ["centos", "redhat", "suse", "fedora"] => { "default" => ["epel-release", "python-pip", "git", "sysstat", "initscripts"] },
    ["debian", "ubuntu"] => { "default" => ["python-pip", "git", "sysstat"] },
    "default" => ["python-pip", "git", "sysstat"]
})

# Installs dependencies
pkg_deps.each do |pkg|
    package pkg do
        action :install
    end
end

directory node['dbmon']['global']['log_dir'] do
    owner "root"
    group "root"
    mode "00750"
    recursive true
    action :create
end

bash "install_dbmon" do
    user "root"
    group "root"
    code <<-EOH
        /usr/bin/pip install git+https://bitbucket.com/psce/dbmon
    EOH
    not_if { ::File.exists?("/usr/bin/dbmon") }
end

template "/etc/dbmon.conf" do
    source "dbmon.conf.erb"
    owner "root"
    group "root"
    mode "0640"
    variables ({
      :global => node['dbmon']['global'],
      :tasks => node['dbmon']['tasks'],
    })
end

execute 'dbmon.service' do
  command 'systemctl start dbmon.service'
  action :nothing
end


if ['redhat', 'centos'].include?node['platform'] 
  if node['platform_version'].to_f >= 7.00
#    systemd_unit 'dbmon.service' do
#      enabled true
#      active  true
#      action  [:create, :start]
#      content "[Unit]\nDescription=dbmon\nAfter=network.target remote-fs.target nss-lookup.target\n\n[Service]\nType=simple\nPIDFile=/var/run/dbmon.pid\nExecStart=/usr/bin/dbmon -c /etc/dbmon.conf -p /var/run/dbmon.pid -f\nExecReload=/bin/kill -s HUP $MAINPID\nExecStop=/bin/kill -s QUIT $MAINPID\nPrivateTmp=true\n\n[Install]\nWantedBy=multi-user.target\n"
#    end

   template "/etc/systemd/system/dbmon.service" do
    source "dbmon.systemd.c.erb"
    owner "root"
    group "root"
    mode "0640"
    notifies :run, 'execute[dbmon.service]', :immediately 
   end
  else 
     template "/etc/init.d/dbmon" do
      source "dbmon.init.erb"
      user "root"
      owner "root"
      mode "00755"
      action :create
     end
     easy_install_package 'futures' do
      action :install
     end
     easy_install_package 'argparse' do
      action :install
     end
     easy_install_package 'six' do
      action :install
     end
     easy_install_package 'pymysql' do
      version '0.7.5'
      action :install
     end
    service "dbmon" do
     supports :start => true, :stop => true, :restart => true
     action [:enable, :start]
    end
  end
end

if ['debian'].include?node['platform'] 
  if node['platform_version'].to_f >= 8.00
#     systemd_unit 'dbmon.service' do
#      enabled true
#      active  true
#      action  [:create, :start]
#      content "[Unit]\nDescription=dbmon\nAfter=network.target remote-fs.target nss-lookup.target\n\n[Service]\nType=simple\nPIDFile=/var/run/dbmon.pid\nExecStart=/usr/local/bin/dbmon -c /etc/dbmon.conf -p /var/run/dbmon.pid -f\nExecReload=/bin/kill -s HUP $MAINPID\nExecStop=/bin/kill -s QUIT $MAINPID\nPrivateTmp=true\n\n[Install]\nWantedBy=multi-user.target\n"
#     end
   template "/etc/systemd/system/dbmon.service" do
    source "dbmon.systemd.erb"
    owner "root"
    group "root"
    mode "0640"
    notifies :run, 'execute[dbmon.service]', :immediately
   end
  else
     template "/etc/init.d/dbmon" do
      source "dbmon.init.debian.erb"
      user "root"
      owner "root"
      mode "00755"
      action :create
     end
     easy_install_package 'futures' do
      action :install
     end
     easy_install_package 'argparse' do
      action :install
     end
     easy_install_package 'six' do
      action :install
     end
     easy_install_package 'pymysql' do
      version '0.7.5'
      action :install
     end

    service "dbmon" do
     supports :start => true, :stop => true, :restart => true
     action [:enable, :start]
    end
  end
end

if ['ubuntu'].include?node['platform']
  if node['platform_version'].to_f >= 15.04
#     systemd_unit 'dbmon.service' do
#      enabled true
#      active  true
#      action  [:create, :start]
#      content "[Unit]\nDescription=dbmon\nAfter=network.target remote-fs.target nss-lookup.target\n\n[Service]\nType=simple\nPIDFile=/var/run/dbmon.pid\nExecStart=/usr/local/bin/dbmon -c /etc/dbmon.conf -p /var/run/dbmon.pid -f\nExecReload=/bin/kill -s HUP $MAINPID\nExecStop=/bin/kill -s QUIT $MAINPID\nPrivateTmp=true\n\n[Install]\nWantedBy=multi-user.target\n"
#     end
   template "/etc/systemd/system/dbmon.service" do
    source "dbmon.systemd.erb"
    owner "root"
    group "root"
    mode "0640"
    notifies :run, 'execute[dbmon.service]', :immediately
  end
  else
     template "/etc/init.d/dbmon" do
      source "dbmon.init.debian.erb"
      user "root"
      owner "root"
      mode "00755"
      action :create
     end
    service "dbmon" do
     supports :start => true, :stop => true, :restart => true
     action [:enable, :start]
    end
  end
end


#     cookbook_file "/etc/systemd/system/dbmon.service" do
#         source "dbmon.service"
#         owner "root"
#         group "root"
#         mode "00644"
#         action :create
#     end

#     service "dbmon" do
#         provider Chef::Provider::Service::Systemd
#         supports :start => true, :stop => true, :restart => true
#         action [:enable, :start]
#     end
# else
#     cookbook_file "/etc/init.d/dbmon" do
#         source "dbmon.init"
#         user "root"
#         owner "root"
#         mode "00755"
#         action :create
#     end

#     service "dbmon" do
#         supports :start => true, :stop => true, :restart => true
#         action [:enable, :start]
#     end
# end
