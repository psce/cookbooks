default['dbmon']['config'] = "/etc/dbmon.conf"
default['dbmon']['pid'] = "/var/run/dbmon.pid"

default['dbmon']['global']['log_dir'] = "/var/log/dbmon"

default['dbmon']['tasks'] = {}