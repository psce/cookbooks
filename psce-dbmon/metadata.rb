name             'psce-dbmon'
maintainer       'PSCE'
maintainer_email 'maciej@git.posterus.com'
license          'All rights reserved'
description      'Installs/Configures psce-dbmon'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.3'
