name "dbmon"
run_list ["recipe[psce-dbmon]"]
override_attributes "dbmon" => {
   "tasks" => {
       "mpstat" => {
           "type" => "continuous",
           "retries" => 10,
           "command_type" => "shell",
           "command" => "/usr/bin/mpstat -P ALL 1",
       },
       "iostat" => {
           "type" => "continuous",
           "retries" => 10,
           "command_type" => "shell",
           "command" => "/usr/bin/iostat -x 1",
       },
       "vmstat" => {
           "type" => "continuous",
           "retries" => 10,
           "command_type" => "shell",
           "command" => "/usr/bin/vmstat 1",
       },
       "slave_status" => {
           "type" => "periodic",
           "interval" => 60,
           "command_type" => "mysql",
           "command" => "SHOW SLAVE STATUS",
           "mysql_default_file" => "/root/.my.cnf",
       },
       "innodb_status" => {
           "type" => "periodic",
           "interval" => 5,
           "command_type" => "mysql",
           "command" => "SHOW ENGINE INNODB STATUS",
           "mysql_default_file" => "/root/.my.cnf",
       },
       "processlist" => {
           "type" => "periodic",
           "interval" => 5,
           "command_type" => "mysql",
           "command" => "SHOW PROCESSLIST",
           "mysql_default_file" => "/root/.my.cnf",
       },
       "global_variables" => {
           "type" => "periodic",
           "interval" => 5,
           "command_type" => "mysql",
           "command" => "SHOW GLOBAL VARIABLES",
           "mysql_default_file" => "/root/.my.cnf",
       },
       "global_status" => {
           "type" => "periodic",
           "interval" => 5,
           "command_type" => "mysql",
           "command" => "SHOW GLOBAL STATUS",
           "mysql_default_file" => "/root/.my.cnf",
       },
   }
}
